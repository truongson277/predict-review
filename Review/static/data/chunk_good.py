from  underthesea  import  chunk

def data_chunk(text):
    _list = chunk(text)
    results_chunk = []
    for index, val in enumerate(_list):
        if val[1] not in ['R', 'A']: continue
        results_chunk.append(val[0])
    return results_chunk


with open('good/test.txt', 'r') as good_data:
    for index, val in enumerate(good_data):
        line = data_chunk(val.lower())
        line = '/'.join(line)
        if line != '':
            with open('4.txt', 'a+') as good_chunk:
                good_chunk.write(line + '\n')



