from django.test import TestCase
import pandas as pd
# Create your tests here.
# docA = "gần/như/không/có/bị/noise/nhưng/vẫn/được/thu/lại/tốt/tuy nhiên/live/không/được/tích hợp/như/rộng/chụp/dù/lẫn/rối/vẫn/chưa/được/làm/mờ/khiến/thiếu/đi/chuyên nghiệp/tuynhiên/và/a8/+/lại/làm/chưa/thực sự/tốt/rối/nằm/và/trừ/nhẹ/là/led/được/bố"
# docB = "nhưng/phóng/to/lên/có thể/thấy/camera/rộng/bắt/kém/hơn/nhỏ/sát/thường/bị/bệt/không/còn/và/sang trọng/mặc dù/rất/rẻ/chuẩn/và/không/phải/cũng/dùng/nhưng/apple/cũng/sẽ/gây/bất tiện/còn/dùng/cũ/có/khá/nhiều/tản/nhưng/chưa/cao/như/mong đợi"
f = open('test', 'r')
list_ = []
bowA = []
for index, value in enumerate(f):
    bowA = value.split("/")
    for word in bowA:
        list_.append(word.replace('\n', ''))
    # bowB = value.split("/")
print(len(list_))
word_dict = set(list_)
print(len(word_dict))
print(word_dict)
wordDictA = dict.fromkeys(word_dict, 0)
# wordDictB = dict.fromkeys(word_dict, 0)
for word in list_:
    wordDictA[word] += 1

# for word in bowB:
#     wordDictB[word]+=1

print(wordDictA)

def compute_TF(word_dict, bow):
    tf_dict = {}
    bow_count = len(bow)
    for word, count in word_dict.items():
        tf_dict[word] = count / float(bow_count)

    return tf_dict


tf_bowA = compute_TF(wordDictA, list_)
# tf_bowB = compute_TF(wordDictB, bowB)
print(tf_bowA)

def compute_IDF(doc_list):
    import math
    idf_dict = {}
    N = sum(1 for line in open('test'))

    # count number of documents that contain this word
    idf_dict = dict.fromkeys(doc_list[0].keys(), 0)
    for doc in doc_list:
        for word, count in doc.items():
            if count > 0:
                idf_dict[word] += 1

    for word, count in idf_dict.items():
        idf_dict[word] = math.log(N / float(count))

    return idf_dict

idfs = compute_IDF([wordDictA])

print(idfs)
def compute_TFIDF(tf_bow, idfs):
    tfidf = {}
    for word, val in tf_bow.items():
        tfidf[word] = val*idfs[word]
    return tfidf

tfidf_bowA = compute_TFIDF(tf_bowA, idfs)

print(tfidf_bowA)

with open('tf.txt', 'w') as tf:
    for i in tfidf_bowA.items():
        tf.write(str(i))
        tf.write('\n')
# tfidf_bowB = compute_TFIDF(tf_bowB, idfs)
# print(tfidf_bowA)
# print(tfidf_bowB)
#
df = pd.DataFrame([tfidf_bowA])
