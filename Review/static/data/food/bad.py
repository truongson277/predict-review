from  underthesea  import  chunk


def data_chunk(text):
    _list = chunk(text)
    results_chunk = []
    for index, val in enumerate(_list):
        # if val[1] not in ['R', 'A', 'X', 'N', 'V', 'C', 'P']: continue
        if val[1] not in ['R', 'A']: continue
        results_chunk.append(val)
    return results_chunk


with open('/home/truongson/TS/predict-review/Review/static/data/food/bad.txt', 'r') as good_data:
    for index, val in enumerate(good_data):
        line = data_chunk(val.lower())
        # line = '/'.join(line)
        if len(line) >= 2:
            A = []
            for i, val in enumerate(line):
                if val[1] == 'R' and i < len(line) - 2:
                    # print(val)
                    test = line.index(val)
                    if line[test + 1][1] == 'A':
                        word = line[test][0] + ' ' + line[test + 1][0]
                        if word not in A:
                            A.append(word)
            A = '/'.join(A)
            if A != '':
                print(A)
                with open('/home/truongson/TS/predict-review/Review/static/data/food/bad_chunk.txt', 'a') as good_chunk:
                    good_chunk.write(A + '\n')
                good_chunk.close()




