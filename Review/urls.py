from django.urls import path
from . import views

urlpatterns = [

    path('', views.index),
    path('input', views.data),
    path('save', views.save_data),
    path('predict', views.predict_review),
    path('save-predict', views.save_predict),
    path('save-good', views.save_good),
    path('save-bad', views.save_bad),
    path('save-normal', views.save_normal)
]